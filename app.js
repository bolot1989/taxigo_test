var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

server.listen(process.env.PORT || 3000, function() {
    console.log('Server is listening to the port: 3000');
});

app.use(express.static(__dirname +  '/public/'));

var users = {}, countUsers = 0;

io.on('connection', function(socket){
    console.log('A user connected');
    socket.on('setUsername', function(data){
        var userData = Object.keys(users).map(function(v) { return users[v]; });
        var countDuplicates = 0;
        for(var i = 0; i < userData.length; i++) {
            if(userData[i].userName === data)
                countDuplicates++;
        }

        if(countDuplicates > 0) {
            console.log('Name is already taken');
            socket.emit('userExists', data + ' username is taken! Try some other username.');
        }
        else {
            countUsers++;
            users[countUsers] = {userName: data, userStatus: 'no status'};
            socket.emit('userSet', {userName: data, userNumber: countUsers});
            io.sockets.emit('listOfClients', users);
        }
    });

    socket.on('changeUsername', function(data) {
        console.log(users);
        if(users[data.userNum]) {
            users[data.userNum].userName = data.newName;
            io.sockets.emit('listOfClients', users);
        }
    });

    socket.on('changeStatus', function(data) {
        if(users[data.number]) {
            users[data.number].userStatus = data.userStatus;
            io.sockets.emit('listOfClients', users);
        }
    });

    socket.on('msg', function(data){
        var name = users[data.number].userName;
        data.userName = name;
        io.sockets.emit('newmsg', data);
    })

    socket.on('disconnectUser', function(data) {
        var userData = Object.keys(users).map(function(v) { return users[v]; });
        for(var i = 0; i < userData.length; i++) {
            if(userData[i].userName === data) {
                var key = Object.keys(users).filter(function(key) {return users[key] == userData[i]})[0];
                delete users[key];
            }
        }
        io.sockets.emit('listOfClients', users);

    });

    socket.on('disconnect', function () {
        console.log("User disconnected");
    });
});