/**
 * Created by bolot on 7.01.17.
 */
(function() {
    'use strict';

    SocketController.$inject = ['$scope', 'socket', '$window'];
    function SocketController($scope, socket, $window) {
        var userName, userNumber;

        $scope.setUsername = function () {
            if($scope.name) {
                socket.emit('setUsername', $scope.name);

                socket.on('userExists', function(data) {
                    $scope.warning = data;
                 });

                socket.on('userSet', function(data) {
                    userName = data.userName;
                    userNumber = data.userNumber;
                    $scope.name = null;
                    $scope.mainFunctionality = true;
                    $scope.showUsername = false;
                });

            }
        },

        $scope.sendMessage = function() {
            var msg = $scope.message;
            socket.emit('msg', {message: msg, number: userNumber});

        },

        $scope.changeUsername = function() {
            if($scope.nameSwitch) {
                socket.emit('changeUsername',
                    {newName: $scope.nameSwitch, oldName: userName, userNum: userNumber});
            } else {
              alert("Check text box data");
            }
        },

        $scope.changeUserStatus = function() {
            var status = $scope.currentStatus;
            if(status) {
                socket.emit('changeStatus', {number: userNumber, userStatus: status});
            }
        },

         $scope.leaveConversation = function () {
             if($scope.nameSwitch) {
                socket.emit('disconnectUser', $scope.nameSwitch);
             }
             else {
                 socket.emit('disconnectUser', userName);
             }
             $window.location.reload();
         },

        socket.on('listOfClients', function(data) {
            var userData = Object.keys(data).map(function(v) { return data[v]; });
            $scope.users = userData;
        });

        socket.on('newmsg', function(data) {
            $scope.messages.push(data);
        });
    }

    angular.module('socketApp').controller('SocketController', SocketController);
}());